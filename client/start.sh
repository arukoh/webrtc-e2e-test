#!/bin/bash

sudo cp /etc/default/ufw{,.org}
sudo sed -i -e "s/^IPV6=yes$/IPV6=no/g" /etc/default/ufw

sudo ufw default ALLOW
[ -n "$BLOCK_FROM" ] && sudo ufw deny out from any to $BLOCK_FROM port 49152:65535 proto udp
sudo ufw enable
sudo ufw status

/opt/bin/entry_point.sh
