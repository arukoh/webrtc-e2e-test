var path = require('path')

exports.config = {
  host: process.env.SELENIUM_HOST || 'localhost',
  port: Number(process.env.SELENIUM_PORT || '4444'),
  path: '/wd/hub',

  specs: [ path.join(__dirname, 'sender.spec.js') ],
  maxInstances: 1,
  capabilities: {
    browserA: {
      desiredCapabilities: {
        browserName: 'chrome',
        chromeOptions: { args: [
          'disable-web-security',
          'use-fake-device-for-media-stream',
          'use-fake-ui-for-media-stream',
          'use-file-for-fake-video-capture=' + path.join(__dirname, 'fixtures', 'sign_irene_qcif.y4m')
        ]}
      }
    }
  },
  sync: true,
  logLevel: 'silent',
  coloredLogs: true,
  screenshotPath: '/var/log/wdio/',
  baseUrl: 'https://server',
  waitforTimeout: 10000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  plugins: {
    webdriverrtc: {
      browser: 'browserA'
    }
  },
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    timeout: 60000
  }
}
