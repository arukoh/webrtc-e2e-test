var path = require('path')

exports.config = {
  host: process.env.SELENIUM_HOST || 'localhost',
  port: Number(process.env.SELENIUM_PORT || '4444'),
  path: '/wd/hub',

  specs: [ path.join(__dirname, 'receiver.spec.js') ],
  maxInstances: 1,
  capabilities: {
    browserB: {
      desiredCapabilities: {
        browserName: 'chrome',
        chromeOptions: { args: [
          'disable-web-security',
          'use-fake-device-for-media-stream',
          'use-fake-ui-for-media-stream',
          'use-file-for-fake-video-capture=' + path.join(__dirname, 'fixtures', 'silent_qcif.y4m')
        ]}
      }
    }
  },
  sync: true,
  logLevel: 'silent',
  coloredLogs: true,
  screenshotPath: '/var/log/wdio/',
  baseUrl: 'https://server',
  waitforTimeout: 10000,
  connectionRetryTimeout: 90000,
  connectionRetryCount: 3,
  plugins: {
    webdriverrtc: {
      browser: 'browserB'
    }
  },
  framework: 'mocha',
  reporters: ['spec'],
  mochaOpts: {
    ui: 'bdd',
    'timeout': 3600000
  }
}
