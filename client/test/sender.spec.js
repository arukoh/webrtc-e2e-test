'use strict'
const fs = require('fs');
const assert = require('assert');
const path = require('path');

describe('webrtc test', () => {
  const shareDir = '/share';
  const browserA = browser.select('browserA');
  let stats;

  it('should open', () => {
    browser.url('/');
  });

  it('records connection data and prints them out', () => {
    const remote_id = fs.readFileSync(path.resolve(shareDir, 'remote_id'), 'utf-8');
    browserA.setValue('#callto-id', remote_id);
    browserA.click('#make-call').pause(10000);

    try {
      browserA.startAnalyzing(() => {
        var obj = peer.connections;
        return obj[Object.keys(obj)[0]][0].pc;
      });

      var connectionType = browserA.getConnectionInformation();
      console.log(connectionType);

      browser.pause(5000);

      stats = browserA.getStats(5000);
      console.log('mean:', stats.mean);
      console.log('median:', stats.median);
      console.log('max:', stats.max);
      console.log('min:', stats.min);
    }
    catch(e) {
      try { console.log(browserA.log('browser')); } catch(err) { ; };
      throw e;
    }
  });
  /*
  it('check audio input level being lower than 5db', ()  => {
    var inputLevel = stats.max.audio.outbound.inputLevel;
    assert.ok(inputLevel < 5000, 'This was too loud! Your audio input level was ' + inputLevel + '.');
  })
  */
});
