const assert = require('assert');

describe('webrtc p2p test', () => {
  const browserA = browser.select('browserA');
  const browserB = browser.select('browserB');
  let stats;

  it('should open', () => {
    browser.url('/p2p.html');
  });

  it('records connection data and prints them out', () => {
    const remote_id = browserB.getText('#my-id');
    browserA.setValue('#callto-id', remote_id);
    browserA.click('#make-call').pause(1000);

    browserA.startAnalyzing(() => {
      const obj = peer.connections;
      return obj[Object.keys(obj)[0]][0].pc;
    });

    const connectionType = browserA.getConnectionInformation();
    console.log(connectionType);

    browser.pause(5000);

    stats = browserA.getStats(5000);
    console.log('mean:', stats.mean);
    console.log('median:', stats.median);
    console.log('max:', stats.max);
    console.log('min:', stats.min);
  });
  /*
  it('check audio input level being lower than 5db', () => {
    var inputLevel = stats.max.audio.outbound.inputLevel;
    assert.ok(inputLevel < 5000, 'This was too loud! Your audio input level was ' + inputLevel + '.');
  });
  */
});
