'use strict';

const fs = require('fs');
const path = require('path');
const sleep = require('sleep');

describe('Peer', () => {
  const shareDir = '/share';
  const browserB = browser.select('browserB');

  it('should start', () => {
    browser.url('/');

    const remote_id = browserB.getText('#my-id');
    fs.writeFileSync(path.resolve(shareDir, 'remote_id'), remote_id);

    sleep.sleep(Number(process.env.PEER_TIMEOUT || '10'));
  });
});
