# WebRTC E2E testing

# Prepare

Start servers.
```
$ docker-compose up
```

# P2P test

```
# docker-compose exec client1 npm test
```

# TURN test

Start reciever.
```
$ docker-compose exec client2 npm run reciever
```

Start Sender.
```
$ docker-compose exec client2 npm run sender
```
