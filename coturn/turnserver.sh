#!/bin/sh

CONFIG_OPTION=
[ -f "./etc/turnserver.conf" ] || CONFIG_OPTION="-c ./etc/turnserver.conf"

EXTERNAL_IP_OPTION=
if [ -z $SKIP_AUTO_IP ]; then
  if [ ! -z $EXTERNAL_IP ]; then
    EXTERNAL_IP_OPTION="-X $EXTERNAL_IP"
  else
    PUBLIC_IPV4="$(curl -qs http://169.254.169.254/latest/meta-data/public-ipv4)"
    [ -n "$PUBLIC_IPV4" ] || PUBLIC_IPV4="$(curl -qs ipinfo.io/ip)"

    PRIVATE_IPV4="$(curl -qs http://169.254.169.254/latest/meta-data/local-ipv4)"
    [ -n "$PRIVATE_IPV4" ] || PRIVATE_IPV4="$(ip addr show eth0 | grep 'inet ' | awk '{print $2}' | cut -d/ -f1)"

    if [ "${PUBLIC_IPV4}" != "${PRIVATE_IPV4}" ]; then
      EXTERNAL_IP_OPTION="-X $PUBLIC_IPV4/$PRIVATE_IPV4"
    else
      EXTERNAL_IP_OPTION="-X $PUBLIC_IPV4"
    fi
  fi
fi

LOG_OPTIONS=
if [ -z $SKIP_LOG ]; then
  LOG_OPTIONS="-l /var/log/turn/turnserver.log"
fi

COMMAND="/usr/bin/turnserver $CONFIG_OPTION $EXTERNAL_IP_OPTION $LOG_OPTIONS"
echo $COMMAND
exec $COMMAND
